=== Plugin Name ===
Contributors: Bjoern Schiessle
Tags: gnusocial, share, button
Requires at least: 4.4.2
Tested up to: 4.4.2
Stable tag: 0.1
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

This plugin adds a "Share on GNU Social" and "Share on Diaspora"
button at the bottom of your posts.

== Description ==

This plugin adds a "Share on GNU Social" and "Share on Diaspora"
button at the bottom of your posts. The GNU Social part is based on
the gs-share widget by Chimo
(https://code.chromic.org/chimo/gs-share).  It allows you to share to
any GNU Social instance, either as a normal post or as a bookmark.

== Installation ==

1. Unpack `wp-freedom.share.zip` and upload its contents to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
