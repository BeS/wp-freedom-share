<?php
/**
 * Plugin Name: Share on free and decentralized networks (currently GNU Social and Diaspora)
 * Plugin URI: https://gitlab.com/BeS/wp-freedom-share
 * Description: This plugin adds a "Share on GNU Social" and "Share on Diaspora" button at the bottom of your posts.
 * Version: 0.1
 * Author: Bjoern Schiessle
 * Author URI: https://www.schiessle.org
 * License: GPL3
 *
 * Copyright 2016 Bjoern Schiessle <bjoern@schiessle.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

require_once(plugin_dir_path(__FILE__) . 'FreedomShare.php');

new FreedomShare();
