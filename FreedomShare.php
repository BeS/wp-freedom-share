<?php

class FreedomShare {

    private $pluginUrl;
    private $pluginPath;

    public function __construct() {

        $this->pluginUrl = plugin_dir_url(__FILE__);
        $this->pluginPath = plugin_dir_path(__FILE__);


        // Register stylesheets
        // register JavaScript
        
        // show GNU Social button
        


        // Register stylesheets
        add_action('wp_enqueue_scripts', array($this, 'register_share_on_gnusocial_css'));
        add_action('wp_enqueue_scripts', array($this, 'register_share_on_diaspora_css'));
        // register JavaScript
        add_action('wp_enqueue_scripts', array($this, 'register_share_on_gnusocial_js'));
        add_action('wp_enqueue_scripts', array($this, 'register_share_on_diaspora_js'));
        // show Social Network Buttons
        add_filter('the_content',array($this, 'addDiasporaButton'));
        add_filter('the_content',array($this, 'addGnuSocialButton'));
    }

    /**
     * register css files
     */
    public function register_share_on_gnusocial_css() {
        wp_register_style('share-on-gnusocial-gs-share-css', $this->pluginUrl . 'gs-share/css/styles.css');
        wp_enqueue_style('share-on-gnusocial-gs-share-css');
        // additional wordpress specific styles to the stylesheet
        // provided by gs-share
        wp_register_style('share-on-gnusocial-css', $this->pluginUrl . 'style.css', array('share-on-gnusocial-gs-share-css'));
        wp_enqueue_style('share-on-gnusocial-css');
    }

    /**
     * register JavaScript files
     */
    public function register_share_on_gnusocial_js() {
        wp_register_script('share-on-gnusocial-js', $this->pluginUrl . 'gs-share/js/gs-share.js', array('jquery'));
        wp_enqueue_script('share-on-gnusocial-js');
    }

    /**
     * add GNU Social share button at the bottom of the content
     *
     * @param string $content
     * @return string
     */
    public function addGnuSocialButton($content) {
        if ( get_post_type() == 'post' && !is_feed() ) {
            $post = get_post();
            $shareButton =
                "<div ".
                "class=\"gs-share\" " .
                "data-url=\"" . get_permalink($post)."\" " .
                "data-title=\"" . $post->post_title."\">" .
                "<button class=\"js-gs-share gs-share--icon\">" .
                "Share on GNU social</button>" .
                "</div><div class=\"clear\"></div>";
            return $content . $shareButton;
        } else {
            return $content;
        }
    }

    /**
     * register css files for diaspora
     */
    public function register_share_on_diaspora_css() {
        wp_register_style('share-on-diaspora-css', $this->pluginUrl . 'diaspora/css/style.css');
        wp_enqueue_style('share-on-diaspora-css');
    }

    /**
     * register JavaScript files
     */
    public function register_share_on_diaspora_js() {
        wp_register_script('share-on-diaspora-js', $this->pluginUrl . 'diaspora/js/action.js', array('jquery'));
        wp_enqueue_script('share-on-diaspora-js');
    }

    /**
     * add Diaspora share button at the bottom of the content
     *
     * @param string $content
     * @return string
     */
    public function addDiasporaButton($content) {
        if ( get_post_type() == 'post' && !is_feed() ) {
            $post = get_post();

            $shareButton = "<div " .
                "class=\"share-on-diaspora\" " .
                "data-url=\"" . get_permalink($post)."\" " .
                "data-title=\"" . $post->post_title."\">" .
                "<img src=\"".$this->pluginUrl."diaspora/img/diaspora-share.png\"/> " .
                "<span>Share on Diaspora</span> " .
                "</div>";

            return $content . $shareButton;
        } else {
            return $content;
        }
    }

}
