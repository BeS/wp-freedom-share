document.addEventListener('DOMContentLoaded', function () {

    var closest,

    closest = function (elm, cls) {
        while (elm !== document && elm !== null) {
            if (elm.classList.contains(cls)) {
                return elm;
            }

            elm = elm.parentNode;
        }

        return false;
    };

    bindClicks = function () {
        document.addEventListener('click', function (e) {
            var target = e.target,
                lnk = closest(target, 'share-on-diaspora');

            // Don't do anything on right/middle click or if ctrl or shift was pressed while left-clicking
            if (!e.button && !e.ctrlKey && !e.shiftKey && lnk) {
                e.preventDefault();

                shareURL   = lnk.getAttribute('data-url') || window.location.href;
                shareTitle = lnk.getAttribute('data-title') || document.title;

                var url = "https://share.diasporafoundation.org/?title=" + encodeURIComponent(shareTitle) + "&url=" + encodeURIComponent(shareURL);
		var width = 600;
		var height = 400;
		var left = (screen.width/2)-(width/2);
		var top = (screen.height/2)-(height/2);

		window.open(url, 'name', 'width=' + width + ', height=' + height + ', top=' + top + ', left=' + left);

            }
        });
    };

    bindClicks();
});
